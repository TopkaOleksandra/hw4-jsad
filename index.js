// AJAX дозволяє асинхронно оновлювати дані на сторінці (без перезавантаження самої сторінки), через обмін даними з сервером (читати або надсилати їх)


const movies = document.querySelector('.movies');
const loader = document.querySelector('.box_loader')

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(data => {
        data.forEach(movie => {
            const {episodeId, name, openingCrawl} = movie;

            loader.style.display = 'none'

            movies.insertAdjacentHTML('beforeend', `<div class="wrapper">
            <div><b>Episede №: </b>${episodeId}</div>
            <div><b>The name of the movie: </b>${name}</div>
            <div id="movie_${movie.id}"><b>Character information:</b></div>
            <div><b>Brief description: </b>${openingCrawl}</div>
            </div>`)

            movie.characters.forEach(url => {
                fetch(url)
                    .then(response => response.json())
                    .then(personageInfo => {
                        const {name, birthYear, eyeColor, gender, hairColor, height, mass, skinColor} = personageInfo;
                        const correctIdName = name.split(' ').join('')

                        document.getElementById(`movie_${movie.id}`).insertAdjacentHTML('beforeend',
                            `<div>${name}:</div>
                            <span class= ${correctIdName}></span>`)

                        correctInfo('birthday: ', birthYear, movie.id);
                        correctInfo('eye color: ', eyeColor, movie.id);
                        correctInfo('gender: ', gender, movie.id);
                        correctInfo('hair color: ', hairColor, movie.id);
                        correctInfo('height: ', height, movie.id);
                        correctInfo('mass: ', mass, movie.id);
                        correctInfo('skin color: ', skinColor, movie.id);

                        fetch(personageInfo.homeworld)
                            .then(res => res.json())
                            .then(planet => {
                                if (!!planet.name) {
                                    const homeworld = `homeworld: ${planet.name};`
                                    document.querySelectorAll(`.${correctIdName}`)
                                        .forEach(planet => planet.innerText = homeworld)
                                }
                            })
                            .catch(error => console.log('Error in homeworld: ', error))
                    })
                    .catch(error => console.log('Error in personageInfo: ', error))
            })
        })
    })
    .catch(error => console.log('Error in movie : ', error))

async function correctInfo(text, param, place) {
    if (!!param) {
        await document.getElementById(`movie_${place}`).insertAdjacentHTML('beforeend', `<span>${text}${param};</span>`);
    }
}